import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})



export class DataTableComponent implements OnInit {

  // array of all items to be paged
  public data: any;

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  // default value
  selectedRows: number = 10;

  // URL for post
  private _URL = 'https://capco.com/';

  // pager properties
  pageIndex: number;
  pageSize: number;

  constructor(private http: HttpClient) {
    this.pageIndex = 0;
    this.pageSize = 10;
  }


  ngOnInit(): void {
     // get mock data
    this.http.get('assets/sample_data.json')
      .subscribe((data) => {
        setTimeout(() => {
          // set items to json response
          this.data = data;
        }, 2000);
      });
  }
  // post the details
  public send(item) {
    // post mock request
    let request : any;
    request.id = item.id;
    request.status = item.status;
    // try catch for type script  or We can implement RXJS error handling operator 
    try {
      this.http.post<any>(this._URL + 'getUsers', request)
        .subscribe((data) => {
          setTimeout(() => {
            // log the response
            console.log(data);
          }, 2000);
        });
    } catch (error) {
      console.log('> Error is handled: ', error.name);
    }
   
  }

}
