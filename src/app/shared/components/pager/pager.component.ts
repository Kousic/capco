import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-pager',
  // from Parent
  inputs: ['itemCount', 'pageSize', 'pageIndex'],
  // to parent
  outputs: ['pageIndexChange'],
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnInit {
  // get and set  for itemcount
  _itemCount: number;

  // get and set  for pagesize
  _pageSize: number;

  // update the page count
  _pageCount: number;

  // emit the event to parent to know the page index
  _pageIndex: number;
  pageIndexChange = new EventEmitter();

  ngOnInit() {
  }

  constructor() {
    // default
    this.pageSize = 1;
  }
  get itemCount() {
    return this._itemCount;
  }
  set itemCount(value) {
    this._itemCount = value;
    this.updatePageCount();
  }
  get pageSize() {
    return this._pageSize;
  }
  set pageSize(value) {
    this._pageSize = value;
    this.updatePageCount();
  }
  updatePageCount() {
    this._pageCount = Math.ceil(this.itemCount / this.pageSize);
  }
  get pageIndex() {
    return this._pageIndex;
  }
  set pageIndex(value) {
    this._pageIndex = value;
    this.pageIndexChange.emit(this.pageIndex);
  }
  // below method names self explanatory
  get canMoveToNextPage(): boolean {
    return this.pageIndex < this._pageCount - 1 ? true : false;
  }
  get canMoveToPreviousPage(): boolean {
    return this.pageIndex >= 1 ? true : false;
  }

  moveToNextPage() {
    if (this.canMoveToNextPage) {
      this.pageIndex++;
    }
  }

  moveToPreviousPage() {
    if (this.canMoveToPreviousPage) {
      this.pageIndex--;
    }
  }

  moveToLastPage() {
    this.pageIndex = this._pageCount - 1;
  }

  moveToFirstPage() {
    this.pageIndex = 0;
  }

}


