import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './components/data-table/data-table.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PagerComponent } from './components/pager/pager.component';

@NgModule({
  declarations: [DataTableComponent, PagerComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    DataTableComponent,
    HttpClientModule
  ]
})
export class SharedModule { }
